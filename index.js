const { createLogger, format, transports } = require('winston');
const { combine, timestamp} = format;

let guild;
const Discord = require('discord.js')
const client = new Discord.Client()

const EMOJI_EYE = '👀'

const logger = createLogger({
    level: 'debug',
    format: combine(timestamp(), format.json()),
    transports: [
        new transports.File({ filename: './debug.log', level: 'debug' }),
        new transports.File({ filename: './all.log' }),
        new transports.Console()
    ]
});

client.on('ready', () => {
    guild = client.guilds.cache.get(process.env.GUILD_ID)
    console.info('Bot ready to do good stuff')
    client.user.setStatus("online");
})

client.on('message', (receivedMessage) => {
    if (receivedMessage.author === client.user) { // Prevent bot from responding to its own messages
        logger.debug(receivedMessage.content, {username: receivedMessage.author.username, userid: receivedMessage.author.id})
        return
    }

    if (receivedMessage.content.startsWith("&")) {
        logger.warn(receivedMessage.content, {username: receivedMessage.author.username, userid: receivedMessage.author.id})
        receivedMessage.react(EMOJI_EYE)
        receivedMessage.channel.send('I\'m watching you !')
    } else if (receivedMessage.content.startsWith("$")) {
        logger.error(receivedMessage.content, {username: receivedMessage.author.username, userid: receivedMessage.author.id})
    } else {
        logger.info(receivedMessage.content, {username: receivedMessage.author.username, userid: receivedMessage.author.id})
    }


})

client
    .login(process.env.BOT_TOKEN)
    .then(() => console.log('client connected'))
